import React from "react";
import { Route, Redirect } from "react-router-dom";
import useAuth from "../../context/NewFirebaseContext";

export default function ProtectedRoute({ component: Component, ...rest }: any) {
  const { user } = useAuth();
  console.log("rest", rest);
  const { history } = rest;
  return (
    <Route
      render={props =>
        user ? <Component {...props} /> : <Redirect to="/login" />
      }
      {...rest}
    />
  );
}
