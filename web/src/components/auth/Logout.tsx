import React from "react";
import firebase from "../../firebase/config";

export default function Logout(props: any) {
  const { history } = props;
  const handleSubmit = async (e: any) => {
    e.preventDefault();
    try {
      await firebase.auth().signOut();
      alert("You have Signed Out");
      history.push("/login");
    } catch (e) {
      alert(e);
    }
  };

  return (
    <button onClick={handleSubmit} type="button">
      Logout
    </button>
  );
}
