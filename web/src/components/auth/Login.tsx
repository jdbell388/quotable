import React, { useState, useEffect } from 'react';
import { TextField, Button } from '@material-ui/core';
import useAuth from '../../context/NewFirebaseContext';
import firebase from '../../firebase/config';

export default function Login(props: any) {
  const [state, setState] = useState({ email: '', password: '' });
  const { user } = useAuth();
  const { history } = props;

  useEffect(() => {
    if (user) {
      history.push('/dashboard');
    }
  }, []);

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    const { email, password } = state;
    try {
      await firebase.auth().signInWithEmailAndPassword(email, password);
    } catch (e) {
      console.log(e);
      if (e.code === 'auth/invalid-email') {
        alert('You have entered an invalid Email address');
      }
      if (e.code === 'auth/wrong-password') {
        alert('Incorrect email or password. Please try again.');
      }
    }
    history.push('/dashboard');
  };
  const handleChange = (change: any) => {
    console.log(change.target.id);
    const { id } = change.target;
    const { value } = change.target;
    setState({ ...state, [id]: value });
  };
  return (
    <form onSubmit={e => handleSubmit(e)} className="login-form">
      <TextField
        required
        id="email"
        label="Email"
        type="email"
        fullWidth
        margin="normal"
        variant="outlined"
        onChange={handleChange}
        value={state.email}
      />
      <TextField
        required
        id="password"
        label="Password"
        type="password"
        fullWidth
        margin="normal"
        variant="outlined"
        onChange={handleChange}
        value={state.password}
      />

      <Button
        className="login"
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
      >
        Login
      </Button>
    </form>
  );
}
