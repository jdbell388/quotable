import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import ProtectedRoute from '../ProtectedRoute/ProtectedRoute';

const RouteWithLayout = (props: any) => {
  const { layout: Layout, component: Component, ...rest } = props;

  return (
    <ProtectedRoute
      {...rest}
      render={(matchProps: any) => (
        <Layout>
          <Component {...matchProps} />
        </Layout>
      )}
    />
  );
};

export default RouteWithLayout;
