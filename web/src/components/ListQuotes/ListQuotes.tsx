import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableSortLabel,
  TableHead,
  TableRow,
  Button
} from '@material-ui/core';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
    '& .InProgress': {
      color: 'orange'
    },
    '& .todo': {
      color: 'green'
    },
    '& .Complete': {
      color: 'red'
    }
  }
});

interface Data {
  Quote: string;
  Author: string;
  Tags: string;
  Link: string;
}

type Order = 'asc' | 'desc';

function complexSort(obj: any, orderBy: any, sortorder: any) {
  if (sortorder === 'asc') {
    obj.sort((a: any, b: any) => (a.data[orderBy] > b.data[orderBy] ? 1 : -1));
  } else {
    obj.sort((a: any, b: any) => (b.data[orderBy] > a.data[orderBy] ? 1 : -1));
  }

  return obj;
}

export default function ListQuotes(props: any) {
  const classes = useStyles();
  const { quotes } = props;
  const [sortorder, setSortorder] = useState<Order>('asc');
  const [orderBy, setOrderBy] = useState<keyof Data>('Quote');

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data
  ) => {
    const isDesc = orderBy === property && sortorder === 'desc';
    setSortorder(isDesc ? 'asc' : 'desc');
    setOrderBy(property);
  };

  const createSortHandler = (property: keyof Data) => (
    event: React.MouseEvent<unknown>
  ) => {
    handleRequestSort(event, property);
  };

  return (
    <div>
      {quotes !== null ? (
        <TableContainer>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === 'Quote' ? sortorder : false}
                >
                  <TableSortLabel
                    active={orderBy === 'Quote'}
                    direction={sortorder}
                    onClick={createSortHandler('Quote')}
                  >
                    Quote
                  </TableSortLabel>
                </TableCell>
                <TableCell align="right">Author</TableCell>
                <TableCell align="right">Tags</TableCell>
                <TableCell align="right">Link</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {complexSort(quotes, orderBy, sortorder).map((quote: any) => {
                return (
                  <TableRow key={quote.id}>
                    <TableCell scope="row">{quote.data.content}</TableCell>
                    <TableCell align="right">{quote.data.author}</TableCell>
                    <TableCell align="right">
                      {quote.data.tags &&
                        quote.data.tags.map((tag: any) => tag)}
                    </TableCell>
                    <TableCell align="right">
                      <Button
                        component={Link}
                        color="primary"
                        to={`/single/${quote.id}`}
                      >
                        Details
                      </Button>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      ) : (
        <p>no quotes</p>
      )}
    </div>
  );
}
