import React, { useState, useEffect } from 'react';
import { TextField, Button, Typography } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

export default function QuoteForm(props: any) {
  const [content, setContent] = useState<string>();
  const [author, setAuthor] = useState<string>();
  const [tags, setTags] = useState<Array<string>>([]);
  const [singleTag, setSingleTag] = useState<string>('');
  const { submitAction, quote } = props;

  const addTag = () => {
    {
      tags.length > 0 ? setTags([...tags, singleTag]) : setTags([singleTag]);
    }
    setSingleTag('');
  };

  useEffect(() => {
    quote && setContent(quote.content);
    quote && setAuthor(quote.author);
    quote && setTags(quote.tags);
  }, []);

  const submit = async (e: any) => {
    e.preventDefault();
    await submitAction(content, author, tags);
  };

  const removeTag = (tag: string) => {
    console.log(`remove tag ${tag}`);
    const newTags = tags.filter((single: any) => {
      if (single !== tag) {
        return true;
      }
    });
    setTags(newTags);
    return true;
  };

  return (
    <form onSubmit={submit}>
      <TextField
        name="content"
        label="Quote"
        id="content"
        fullWidth
        margin="normal"
        variant="outlined"
        rows="6"
        value={content}
        onChange={(e: any) => {
          const target = e.target as HTMLTextAreaElement;
          setContent(target.value);
        }}
        multiline
      />

      <TextField
        type="text"
        name="author"
        id="author"
        label="Author"
        fullWidth
        margin="normal"
        variant="outlined"
        value={author}
        onChange={(e: any) => {
          const target = e.target as HTMLTextAreaElement;
          setAuthor(target.value);
        }}
      />
      <Typography>
        Tags:
        {tags.length > 0 &&
          tags.map((tag: string) => {
            return (
              <span className="single-tag" key={tag}>
                {tag}
                <button type="button" onClick={(e: any) => removeTag(tag)}>
                  X
                </button>
              </span>
            );
          })}
      </Typography>
      <TextField
        type="text"
        name="tags"
        id="tags"
        label="Tags"
        fullWidth
        margin="normal"
        variant="outlined"
        value={singleTag}
        onChange={(e: any) => {
          const target = e.target as HTMLTextAreaElement;
          setSingleTag(target.value);
        }}
      />
      <Button onClick={addTag}>
        <AddIcon />
      </Button>
      <Button type="submit" fullWidth variant="contained" color="primary">
        Submit
      </Button>
    </form>
  );
}
