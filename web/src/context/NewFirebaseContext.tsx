import React from "react";
import firebase from "../firebase/config";
import "firebase/auth";

export function AuthProvider({ initialUser, children }: any) {
  const [state, dispatch] = React.useReducer(reducer, {
    isInitiallyLoading: true,
    isLoading: false,
    user: null
  });

  const signingInSoDontDispatchOnAuthStateChange = React.useRef(false);
  React.useEffect(() => {
    // Setup Firebase authentication state observer and get user data.

    firebase.auth().onAuthStateChanged((user: any) => {
      if (user) {
        // User is signed in.
        if (signingInSoDontDispatchOnAuthStateChange.current) {
          signingInSoDontDispatchOnAuthStateChange.current = false;
          return;
        }

        dispatch({
          type: AuthProvider.actions.setUser,
          payload: {
            user
          }
        });
      } else {
        // User is signed out.
        dispatch({
          type: AuthProvider.actions.setUser,
          payload: {
            user: null
          }
        });
      }
    });
  }, []);

  const toggleLoading = (isLoading: any) => {
    dispatch({
      type: AuthProvider.actions.toggleLoading,
      payload: {
        value: isLoading
      }
    });
  };

  const signup = (email: any, password: any, displayName: any) => {
    signingInSoDontDispatchOnAuthStateChange.current = true;

    toggleLoading(true);

    let user: any;

    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        user = firebase.auth().currentUser;
        user.sendEmailVerification();
      })
      .then(() => {
        user.updateProfile({
          displayName
        });
      })
      .then(() => {
        toggleLoading(false);
        // Set user with displayName here because user.updateProfile
        // is async and our onAuthStateChanged listener will fire
        // before the user is updated. When that happens, user.displayName
        // value will be null.
        // Reference: https://github.com/firebase/firebaseui-web/issues/36
        const updatedUserWithDisplayName = {
          ...user,
          displayName
        };

        dispatch({
          type: AuthProvider.actions.setUser,
          payload: {
            user: updatedUserWithDisplayName
          }
        });
      })
      .catch((error: any) => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;

        console.log("errorCode", errorCode, "errorMessage", errorMessage);
        toggleLoading(false);
      });
  };

  const signin = (email: any, password: any) => {
    toggleLoading(true);

    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        toggleLoading(false);
      })
      .catch((error: any) => {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log("errorCode", errorCode, "errorMessage", errorMessage);
        toggleLoading(false);
      });
  };

  const signout = () => {
    toggleLoading(true);

    firebase
      .auth()
      .signOut()
      .then(() => {
        // Sign-out successful.
        toggleLoading(false);
      })
      .catch((error: any) => {
        // An error happened.
        toggleLoading(false);
      });
  };

  const sendResetPasswordEmail = (email: any) => {
    toggleLoading(true);

    firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then(() => {
        // Email sent.
        toggleLoading(true);
        // TODO: Toggle success notification here.
      })
      .catch((error: any) => {
        // An error happened.
        console.log("error", error);
        toggleLoading(false);
      });
  };

  const value = {
    user: initialUser || state.user,
    signup,
    signin,
    signout,
    sendResetPasswordEmail,
    isLoading: state.isLoading
  };

  return state.isInitiallyLoading ? (
    <h1>Loading</h1>
  ) : (
    <AuthContext.Provider value={value as any}>{children}</AuthContext.Provider>
  );
}

AuthProvider.actions = {
  setUser: "SET_USER",
  toggleLoading: "TOGGLE_LOADING"
};

const reducer = (state: any, action: any) => {
  switch (action.type) {
    case AuthProvider.actions.setUser:
      return {
        user: action.payload.user,
        isInitiallyLoading: false,
        isLoading: false
      };
    case AuthProvider.actions.toggleLoading:
      return {
        ...state,
        isLoading: action.payload.value
      };
    default:
      throw new Error(`No case for type ${action.type} found.`);
  }
};

const AuthContext = React.createContext(undefined);

export default function useAuth() {
  const context = React.useContext(AuthContext);

  if (context === undefined) {
    throw new Error("useAuth must be used within an AuthProvider");
  }

  return context;
}
