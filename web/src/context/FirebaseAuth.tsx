import React, {
  createContext,
  useReducer,
  useEffect,
  useContext,
  useMemo,
  useState
} from "react";
import firebase from "../firebase/config";

interface FirebaseContext {
  authStatusReported: boolean;
  isUserSignedIn: boolean;
  context: any;
  dispatch: any;
}

const reducer = (state: any, action: any) => {
  switch (action.type) {
    case "UPDATE_AUTH":
      console.log("auth action", action.payload);
      return {
        ...action.payload
      };

    default:
      return state;
  }
};

const defaultFirebaseContext: FirebaseContext = {
  authStatusReported: false,
  isUserSignedIn: false,
  context: [],
  dispatch: []
};
const FirebaseAuthContext = createContext({} as FirebaseContext);

export const FirebaseAuth = (children: any) => {
  const [state, dispatch] = useReducer(
    reducer,
    defaultFirebaseContext as never
  );

  const onChange = (user: any) => {
    console.log("change");
    if (user) {
      dispatch({
        type: "UPDATE_AUTH",
        payload: {
          authStatusReported: true,
          isUserSignedIn: !!user
        }
      });
      localStorage.setItem("authUser", JSON.stringify(user));
    } else {
      dispatch({
        type: "UPDATE_AUTH",
        payload: {
          authStatusReported: false,
          isUserSignedIn: false
        }
      });
      localStorage.removeItem("authUser");
    }
  };

  useEffect(() => {
    console.log("state", state);
    const unsubscribe = firebase.auth().onAuthStateChanged(onChange);
    console.log("unsubscribe", unsubscribe);
    // unsubscribe to the listener when unmounting
    return () => unsubscribe();
  }, []);

  const value = useMemo(() => ({ context: state, dispatch }), [state]);

  return <FirebaseAuthContext.Provider value={value} {...children} />;
};

export const useFirebaseContext = () => useContext(FirebaseAuthContext);
