import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";
import "firebase/storage";

// Initialize Firebase
const config = {
  apiKey: "AIzaSyBOAr6S5S-dyrsoQW2uI1uvpB-7ueVsgew",
  authDomain: "quotable-32a2e.firebaseapp.com",
  databaseURL: "https://quotable-32a2e.firebaseio.com",
  projectId: "quotable-32a2e",
  storageBucket: "",
  messagingSenderId: "583293444198",
  appId: "1:583293444198:web:6428ea3f04cdf179"
};

firebase.initializeApp(config);
// const firestore = firebase.firestore();
// const settings = { timestampsInSnapshots: true };
// firestore.settings(settings);

export default firebase;
