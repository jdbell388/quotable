import firebase from './config';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';

export const addQuote = async ({ quote, author, tags }: any) => {
  const db = firebase.firestore();
  try {
    const addDoc = await db
      .collection('quotes')
      .add({ content: quote, author, tags });
    return addDoc;
  } catch (e) {
    console.log('Error: ', e);
  }
  return {};
};

export const getAllQuotes = async () => {
  const db = firebase.firestore();
  try {
    const quoteList = await db.collection('quotes').get();
    const results: {}[] = [];
    quoteList.forEach(doc => {
      results.push({ id: doc.id, data: doc.data() });
    });
    return results;
  } catch (e) {
    console.log('Error getting quotes: ', e);
  }
  return [];
};

export const getSingleQuote = async (id: any) => {
  const db = firebase.firestore();
  try {
    const quoteList = await db
      .collection('quotes')
      .doc(id)
      .get();
    return quoteList.data();
  } catch (e) {
    console.log('Error getting quote: ', e);
  }
  return {};
};

export const deleteQuote = async (id: any) => {
  const db = firebase.firestore();
  try {
    await db
      .collection('quotes')
      .doc(id)
      .delete();
    return 200;
  } catch (e) {
    console.log('Error deleting quote: ', e);
  }
  return [];
};

export interface QuoteInt {
  content?: string;
  tags?: [''];
  author?: string;
  id?: string;
}

export const updateQuote = async ({ quote, author, tags, id }: any) => {
  const db = firebase.firestore();

  try {
    const quoteRef = await db.collection('quotes').doc(id);
    const updateDoc = await quoteRef.update({
      content: quote,
      author,
      tags
    });
    return updateDoc;
  } catch (e) {
    console.log('Error: ', e);
  }
  return {};
};

export const logout = async () => {
  try {
    await firebase.auth().signOut();
    alert('You have Signed Out');
  } catch (e) {
    alert(e);
  }
};

export const searchByTag = async (term: any) => {
  const db = firebase.firestore();
  try {
    const results: any[] = [];
    const quoteList = await db
      .collection('quotes')
      .where('tags', 'array-contains', term)
      .get()
      .then(function(snapshot) {
        snapshot.forEach(function(doc) {
          results.push({ id: doc.id, data: doc.data() });
        });
      })
      .catch(function(error) {
        console.log('Error getting documents: ', error);
      });
    return results;
  } catch (e) {
    console.log('Error getting quote: ', e);
  }
  return {};
};
