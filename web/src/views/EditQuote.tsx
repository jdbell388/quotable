import React, { useEffect, useState } from 'react';
import { Paper, Grid } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import QuoteForm from '../components/QuoteForm/QuoteForm';
import { getSingleQuote, QuoteInt, addQuote } from '../firebase/functions';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(2),
      display: 'flex',
      overflow: 'auto',
      flexDirection: 'column'
    }
  })
);

export default function EditQuote(props: any) {
  const [quoteObj, setQuoteObj] = useState<QuoteInt | undefined>(undefined);
  const { history } = props;
  const { id } = props.match.params;
  const classes = useStyles();

  const getQuote = async () => {
    const result = await getSingleQuote(id);
    console.log('results', result);
    setQuoteObj({ ...result, id });
  };

  const submitForm = async (quote: any, author: any, tags: any) => {
    const add = await addQuote({ quote, author, tags });
    console.log('add', add);
    alert('updated');
    history.push('/dashboard');
  };

  useEffect(() => {
    getQuote();
  }, []);

  return (
    <>
      <Grid container item>
        <h1>Edit a Quote</h1>
      </Grid>
      <Grid container item>
        <Paper className={classes.paper}>
          {quoteObj && (
            <QuoteForm {...props} quote={quoteObj} submitAction={submitForm} />
          )}
        </Paper>
      </Grid>
    </>
  );
}
