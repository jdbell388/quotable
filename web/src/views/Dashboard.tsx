import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Paper, Fab, Grid } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';

import ListQuotes from '../components/ListQuotes/ListQuotes';
import { getAllQuotes } from '../firebase/functions';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(2),
      display: 'flex',
      overflow: 'auto',
      flexDirection: 'column'
    },
    newIcon: {
      justifySelf: 'flex-end'
    }
  })
);

export default function Dashboard() {
  const classes = useStyles();
  const [quotes, setQuotes] = useState();

  const getQuotes = async () => {
    const results = await getAllQuotes();
    console.log('results', results);
    setQuotes(results);
  };

  useEffect(() => {
    getQuotes();
  }, []);

  return (
    <>
      <Grid container item justify="space-between">
        <Grid item>
          <h1>Dashboard</h1>
        </Grid>
        <Grid item>
          <Fab
            color="primary"
            aria-label="add"
            className={classes.newIcon}
            component={Link}
            to="/add"
          >
            <AddIcon />
          </Fab>
        </Grid>
      </Grid>
      <Grid container item>
        {quotes && (
          <Paper className={classes.paper}>
            <ListQuotes quotes={quotes} />
          </Paper>
        )}
      </Grid>
    </>
  );
}
