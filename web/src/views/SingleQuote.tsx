import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Button,
  Card,
  Grid,
  CardContent,
  Typography,
  CardActions
} from '@material-ui/core';

import { getSingleQuote, QuoteInt, deleteQuote } from '../firebase/functions';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(2),
      display: 'flex',
      overflow: 'auto',
      flexDirection: 'column'
    }
  })
);

export default function SingleQuote(props: any) {
  const [quote, setQuote] = useState<QuoteInt | undefined>(undefined);
  const { history } = props;
  const { id } = props.match.params;
  const classes = useStyles();

  const getQuote = async () => {
    const result = await getSingleQuote(id);
    console.log('results', result);
    setQuote(result);
  };
  const removeQuote = async () => {
    await deleteQuote(id);
    alert('Quote Deleted');
    history.push('/dashboard');
  };
  useEffect(() => {
    getQuote();
  }, []);
  return (
    <Grid container item>
      {quote && (
        <Card className={classes.paper}>
          <CardContent>
            {quote.content && <Typography>{quote.content}</Typography>}
            {quote.author && <Typography>Author: {quote.author}</Typography>}
            {quote.tags && <Typography>{quote.tags.map((tag: string) => `${tag}, `)}</Typography>}
          </CardContent>
          <CardActions>
            <Button component={Link} to={`/edit/${id}`}>
              Edit
            </Button>
          </CardActions>
        </Card>
      )}
    </Grid>
  );
}
