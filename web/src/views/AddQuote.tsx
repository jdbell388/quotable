import React from 'react';
import { Paper, Grid } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import QuoteForm from '../components/QuoteForm/QuoteForm';
import { addQuote } from '../firebase/functions';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(2),
      display: 'flex',
      overflow: 'auto',
      flexDirection: 'column'
    }
  })
);

export default function AddQuote(props: any) {
  const classes = useStyles();
  const { history } = props;

  const submitForm = async (quote: any, author: any, tags: any) => {
    const add = await addQuote({ quote, author, tags });
    console.log('add', add);
    alert('added');
    history.push('/dashboard');
  };

  return (
    <>
      <Grid container item>
        <h1>Add a new Quote</h1>
      </Grid>
      <Grid container item>
        <Paper className={classes.paper}>
          <QuoteForm {...props} submitAction={submitForm} />
        </Paper>
      </Grid>
    </>
  );
}
