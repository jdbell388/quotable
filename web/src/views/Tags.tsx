import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Button,
  Card,
  Grid,
  CardContent,
  Typography,
  CardActions,
  Paper,
  TextField
} from '@material-ui/core';
import { searchByTag, getAllQuotes } from '../firebase/functions';
import ListQuotes from '../components/ListQuotes/ListQuotes';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      padding: theme.spacing(2),
      display: 'flex',
      overflow: 'auto',
      flexDirection: 'column'
    }
  })
);

const Tags = () => {
  const classes = useStyles();
  const [term, setTerm] = useState('');
  const [results, setResults] = useState();

  const search = async (searchTerm: string) => {
    const searchResults = await searchByTag(searchTerm);
    console.log('searchresults', searchResults);
    setResults(searchResults);
  };

  const getQuotes = async () => {
    const allResults = await getAllQuotes();
    setTerm('');
    setResults(allResults);
  };

  useEffect(() => {
    getQuotes();
  }, []);

  return (
    <>
      <Grid container item justify="space-between">
        <Grid item>
          <h1>Tags</h1>
        </Grid>
      </Grid>
      <Grid container item>
        <TextField
          type="text"
          name="term"
          id="term"
          label="Search"
          fullWidth
          margin="normal"
          variant="outlined"
          value={term}
          onChange={(e: any) => {
            const target = e.target as HTMLTextAreaElement;
            setTerm(target.value);
          }}
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          onClick={() => search(term)}
        >
          Submit
        </Button>
        <Button
          fullWidth
          variant="contained"
          color="secondary"
          onClick={() => getQuotes()}
        >
          Clear
        </Button>
      </Grid>
      <Grid container item>
        {results && (
          <Paper className={classes.paper}>
            <ListQuotes quotes={results} />
          </Paper>
        )}
      </Grid>
    </>
  );
};
export default Tags;
