import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import firebase from '../firebase/config';
import 'firebase/auth';

const Logout = ({ history }: any) => {
  const logoutWithRedirect = async () => {
    await firebase.auth().signOut();
    alert('You have Signed Out');
    await history.push('/dashboard');
  };

  useEffect(() => {
    logoutWithRedirect();
  });
  return (
    <>
      <h3>Logging Out...</h3>
    </>
  );
};

export default withRouter(Logout);
