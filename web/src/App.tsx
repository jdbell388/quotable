import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from './components/auth/Login';
import { AuthProvider } from './context/NewFirebaseContext';
import Dashboard from './views/Dashboard';
import AddQuote from './views/AddQuote';
import ProtectedRoute from './components/ProtectedRoute/ProtectedRoute';
import SingleQuote from './views/SingleQuote';
import EditQuote from './views/EditQuote';
import Tags from './views/Tags';
import RouteWithLayout from './components/RouteWithLayout';
import { Main as MainLayout } from './layouts/index';

const App: React.FC = () => {
  return (
    <Router>
      <AuthProvider>
        <Switch>
          <RouteWithLayout exact path="/add" component={AddQuote} layout={MainLayout} />
          <RouteWithLayout path="/edit/:id" component={EditQuote} layout={MainLayout} />
          <RouteWithLayout path="/single/:id" component={SingleQuote} layout={MainLayout} />
          <RouteWithLayout path="/tags" component={Tags} layout={MainLayout} />
          <RouteWithLayout exact path="/dashboard" component={Dashboard} layout={MainLayout} />
          <RouteWithLayout path="/" render={(props: any) => <Login {...props} />} layout={MainLayout} />
        </Switch>
      </AuthProvider>
    </Router>
  );
};

export default App;
