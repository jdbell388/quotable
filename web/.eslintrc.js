module.exports = {
  parser: '@typescript-eslint/parser',
  env: {
    browser: true,
    es6: true
  },
  settings: {
    ecmascript: 6,
    jsx: true,
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx']
      }
    }
  },
  parserOptions: {
    ecmaVersion: 2017,
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      experimentalDecorators: true,
      jsx: true
    },
    sourceType: 'module'
  },
  plugins: ['@typescript-eslint', 'react', 'prettier'],
  extends: [
    'airbnb',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:prettier/recommended'
  ],
  rules: {
    'react/jsx-filename-extension': 0,
    'function-paren-newline': 0,
    'react/no-did-mount-set-state': 0,
    'react/prefer-stateless-function': 0,
    'import/prefer-default-export': 0,
    'no-tabs': 0,
    'no-mixed-spaces-and-tabs': 0,
    'prettier/prettier': 'error',
    'import/no-extraneous-dependencies': 0,
    quotes: [2, 'single', { avoidEscape: true }]
  }
};
